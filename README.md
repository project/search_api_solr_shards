# Search API Solr Shards

This Drupal module extends the functionality of the Solr Search API, allowing distributed searches across multiple cores, with compatibility for facets. This helps to enable efficient and scalable searches across distributed systems.

**Features**
- Distributed searches across multiple Solr cores.
- Compatibility with facets in distributed searches.
- Extends the Search API Solr module with the shard functionality.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/search_api_solr_shards).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search_api_solr_shards).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This project requires Drupal core and the [Search API Solr](https://www.drupal.org/project/search_api_solr) module. No additional dependencies are needed.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

To configure Search API Solr Shards:
1. Ensure that you have multiple Solr cores set up.
2. Configure shard parameters in the Solr index settings.
3. Optionally configure facets for distributed searches.

## Maintainers

- Daniel Cimorra - [dcimorra](https://www.drupal.org/u/dcimorra)
