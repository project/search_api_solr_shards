<?php

namespace Drupal\search_api_solr_shards\Plugin\search_api\datasource;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api_solr\Plugin\search_api\datasource\SolrDocument;

/**
 * Represents a datasource which exposes external Solr Documents.
 *
 * @SearchApiDatasource(
 *   id = "solr_document",
 *   label = @Translation("Solr Document"),
 *   description = @Translation("Search through external Solr content with shards and aliases compatiblity. (Only works if this index is attached to a Solr-based server.)"),
 * )
 */
class ShardDocument extends SolrDocument {

  /**
   * Solr field property name.
   *
   * @var string
   */
  protected $solrField = 'shards_field';

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['enable_shards'] = [
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['enable_shards'],
      '#title' => 'Enable shards search',
      '#description' => 'If enable, fields and results will be retrieve from shards configured servers.'
    ];
    $form['solrconfig'] = [
      '#type' => 'textfield',
      '#default_value' => $this->configuration['solrconfig'],
      '#title' => $this->t('Solrconfig.xml file'),
      '#description' => $this->t('The file from which the shard property will be read'),
    ];

    return $form + parent::buildConfigurationForm($form, $form_state);
  }

}
