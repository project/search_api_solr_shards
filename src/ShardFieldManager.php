<?php

namespace Drupal\search_api_solr_shards;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\UseCacheBackendTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\search_api\Entity\Server;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\LoggerTrait;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_solr\SearchApiSolrException;
use Drupal\search_api_solr\SolrBackendInterface;
use Drupal\search_api_solr\SolrConnectorInterface;
use Drupal\search_api_solr\SolrFieldManager;
use Drupal\search_api_solr\TypedData\SolrFieldDefinition;
use Exception;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Manages the discovery of Solr fields.
 */
class ShardFieldManager extends SolrFieldManager {

  use UseCacheBackendTrait;
  use StringTranslationTrait;
  use LoggerTrait;

  /**
   * Constants for configuration and paths.
   */
  private const DEFAULT_SOLR_CONFIG_FILE = 'solrconfig_extra.xml';
  private const SHARDS_ATTRIBUTE = 'shards';
  private const LUKE_QUERY_SUFFIX = '/admin/luke?wt=json';

  /**
   * Static cache of field definitions per Solr server.
   *
   * @var array
   */
  protected $fieldDefinitions;

  /**
   * Storage for Search API servers.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $serverStorage;

  /** @var \GuzzleHttp\ClientInterface */
  protected $httpClient;

  /**
   * Constructs a new ShardFieldManager.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger for Search API.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   HTTP client for shard communication.
   *
   * @throws \InvalidArgumentException
   */
  public function __construct(CacheBackendInterface $cache_backend, EntityTypeManagerInterface $entityTypeManager, LoggerInterface $logger, ClientInterface $httpClient) {
    if (!$httpClient instanceof ClientInterface) {
      throw new \InvalidArgumentException('Invalid HTTP client provided.');
    }
    parent::__construct($cache_backend, $entityTypeManager, $logger);
    $this->httpClient = $httpClient;
  }

  /**
   * Builds the field definitions for a Solr server.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index from which we are retrieving field information.
   *
   * @return \Drupal\Core\TypedData\DataDefinitionInterface[]
   *   The array of field definitions for the server, keyed by field name.
   *
   * @throws \InvalidArgumentException
   * @throws \Drupal\search_api\SearchApiException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function buildFieldDefinitions(IndexInterface $index) {
    $solr_fields = [];

    if ($index->isNew() && !$index->getServerInstance()) {
      return [];
    }

    /** @var Server $server */
    $server = $index->getServerInstance();
    if ($server === NULL) {
      throw new \InvalidArgumentException('The Search API server could not be loaded.');
    }

    if (!$server->status()) {
      return $solr_fields;
    }

    $backend = $server->getBackend();
    if (!$backend instanceof SolrBackendInterface) {
      throw new \InvalidArgumentException("The Search API server's backend must be an instance of SolrBackendInterface.");
    }

    try {
      $connector = $this->getConnector($server);
      $is_shard_enabled = $this->isShardEnabled($index);

      if (str_contains($connector->getPluginId(), 'solr_cloud') && !$is_shard_enabled) {
        $solr_fields = $this->buildFieldDefinitionsFromSolrCloud($server, $connector);
      }
      elseif ($is_shard_enabled) {
        $solr_fields = $this->buildFieldDefinitionsFromSolrShards($index, $server);
      } else {
        $solr_fields = parent::buildFieldDefinitionsFromSolr($index);
      }

      $config_fields = $this->buildFieldDefinitionsFromConfig($index);
      $fields = $solr_fields + $config_fields;
      foreach ($config_fields as $key => $field) {
        $fields[$key]->setDataType($field->getDataType());
      }
      return $fields;
    }
    catch (SearchApiSolrException $e) {
      $this->getLogger()->error('Could not connect to server %server, %message', [
        '%server' => $server->id(),
        '%message' => $e->getMessage(),
      ]);
    }

    return $solr_fields;
  }

  /**
   * Builds the field definitions for a Solr server from its Luke handler.
   *
   * @param \Drupal\search_api\Entity\Server $server
   *   The Solr server.
   * @param \Drupal\search_api_solr\SolrConnectorInterface $connector
   *   The Solr connector.
   *
   * @return \Drupal\Core\TypedData\DataDefinitionInterface[]
   *   The array of field definitions for the server, keyed by field name.
   *
   * @throws \Drupal\search_api\SearchApiException
   * @throws \InvalidArgumentException
   */
  protected function buildFieldDefinitionsFromSolrCloud(Server $server, SolrConnectorInterface $connector) {
    $fields = [];

    try {
      $lukes = [];
      $aliases = $connector->getClusterStatus()->getAliases();
      $core = $server->getBackendConfig()['connector_config']['core'];

      if (array_key_exists($core, $aliases)) {
        $collections = explode(',', $aliases[$core]);
        foreach ($collections as $collection) {
          $backendConfig = $server->getBackendConfig();
          $backendConfig['connector_config']['core'] = $collection;
          $server->setBackendConfig($backendConfig);

          $connector = $server->getBackend()->getSolrConnector();
          $lukes[] = $connector->getLuke();
        }
      }

      if (empty($lukes)) {
        $lukes[] = $connector->getLuke();
      }

      foreach ($lukes as $luke) {
        foreach ($luke['fields'] as $name => $definition) {
          $field = new SolrFieldDefinition($definition);
          $field->setLabel(Unicode::ucfirst(trim(str_replace('_', ' ', $name))));
          $field->setDataType($this->mapSolrFieldType($field->getDataType()));
          $fields[$name] = $field;
        }
      }
    }
    catch (SearchApiSolrException $e) {
      $this->getLogger()->error('Could not connect to Solr cloud server %server, %message', [
        '%server' => $server->id(),
        '%message' => $e->getMessage(),
      ]);
    }

    return $fields;
  }

  /**
   * Builds the field definitions for a Solr server from its Luke handler.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index from which we are retrieving field information.
   *
   * @return \Drupal\Core\TypedData\DataDefinitionInterface[]
   *   The array of field definitions for the server, keyed by field name.
   *
   * @throws \InvalidArgumentException
   * @throws \Drupal\search_api\SearchApiException
   */
  protected function buildFieldDefinitionsFromSolrShards(IndexInterface $index, Server $server) {
    $fields = [];
    $shards = $this->getShardServers($index);

    foreach ($shards as $shard) {
      try {
        $response = $this->httpClient->get($shard);
        $shardFields = json_decode((string) $response->getBody(), TRUE)['fields'];
        foreach ($shardFields as $name => $definition) {
          $field = new SolrFieldDefinition($definition);
          $field->setLabel(Unicode::ucfirst(trim(str_replace('_', ' ', $name))));
          $field->setDataType($this->mapSolrFieldType($field->getDataType()));
          $fields[$name] = $field;
        }
      }
      catch (Exception $e) {
        $this->logger->error('Could not receive shard servers or their fields, %message', [
          '%server' => $server->id(),
          '%message' => $e->getMessage(),
        ]);
      }
    }

    return $fields;
  }

  /**
   * Retrieves the Solr connector from the backend.
   *
   * @param \Drupal\search_api\Entity\Server $server
   *   The Solr server.
   *
   * @return \Drupal\search_api_solr\SolrConnectorInterface
   *   The Solr connector.
   *
   * @throws \InvalidArgumentException
   */
  protected function getConnector(Server $server) {
    $backend = $server->getBackend();
    if (!$backend instanceof SolrBackendInterface) {
      throw new \InvalidArgumentException("The Search API server's backend must be an instance of SolrBackendInterface.");
    }
    return $backend->getSolrConnector();
  }

  /**
   * Maps a Solr field type to a Search API data type.
   *
   * @param string $type
   *   The Solr field type.
   *
   * @return string
   *   The corresponding Search API data type.
   *
   * @throws \InvalidArgumentException
   */
  protected function mapSolrFieldType($type) {
    if (!is_string($type)) {
      throw new \InvalidArgumentException('Field type must be a string.');
    }

    if (strpos($type, 'text') !== FALSE) {
      return 'search_api_text';
    }
    if (strpos($type, 'date_range') !== FALSE) {
      return 'solr_date_range';
    }
    if (strpos($type, 'date') !== FALSE) {
      return 'date';
    }
    if (strpos($type, 'int') !== FALSE || strpos($type, 'long') !== FALSE) {
      return 'integer';
    }
    if (strpos($type, 'float') !== FALSE || strpos($type, 'double') !== FALSE) {
      return 'float';
    }
    if (strpos($type, 'bool') !== FALSE) {
      return 'boolean';
    }
    return 'string';
  }

  /**
   * Extracts shard servers from solrconfig.xml.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index for which shards are being retrieved.
   *
   * @return array
   *   The array of shard server URLs.
   */
  protected function getShardServers(IndexInterface $index) {
    $shards = [];

    if ($index->getServerInstance()) {
      try {
        $shardDocument = $index->getDatasource('solr_document');
        $solrconfig = empty($shardDocument->getConfiguration()['solrconfig'])
          ? self::DEFAULT_SOLR_CONFIG_FILE
          : $shardDocument->getConfiguration()['solrconfig'];
      }
      catch (SearchApiException $e) {
        $solrconfig = self::DEFAULT_SOLR_CONFIG_FILE;
      }

      try {
        $server = $index->getServerInstance();
        $connector = $server->getBackend()->getSolrConnector();
        $solrconfigContent = $connector->getFile($solrconfig)->getBody();
        $xml = simplexml_load_string("<conf>" . $solrconfigContent . "</conf>");

        $shards = ((array) $xml->xpath('//*[@name="' . self::SHARDS_ATTRIBUTE . '"]')[0])[0];
        if ($shards) {
          $shards = explode(',', $shards);
        }

        foreach ($shards as &$shard) {
          $shard .= self::LUKE_QUERY_SUFFIX;
        }
      }
      catch (Exception $e) {
        $this->logger->error('Could not retrieve shard servers, %message', [
          '%message' => $e->getMessage(),
        ]);
      }
    }

    return $shards;
  }

  /**
   * Checks if shard support is enabled for the index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index to check.
   *
   * @return bool
   *   TRUE if shard support is enabled, FALSE otherwise.
   */
  protected function isShardEnabled(IndexInterface $index) {
    try {
      $datasource = $index->getDatasource('solr_document');
      return $datasource && !empty($datasource->getConfiguration()['enable_shards']);
    }
    catch (SearchApiException $e) {
      return FALSE;
    }
  }
}
